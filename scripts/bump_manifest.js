/* eslint-disable no-console */
import fs from "fs";

const packageJson = JSON.parse(fs.readFileSync("package.json", "utf-8"));
const manifest = JSON.parse(fs.readFileSync("public/manifest.json", "utf-8"));
const oldManifestVersion = manifest.version;
manifest.version = packageJson.version;

const stringifiedManifest = JSON.stringify(manifest, null, 2);

fs.writeFile("public/manifest.json", stringifiedManifest, "utf-8", err => {
  if (err) {
    console.log("Error when trying to bump manifest version");
    return console.log("error");
  }

  console.log(
    `Bump manifest version from v${oldManifestVersion} to v${manifest.version}`
  );
});

console.log(manifest.version);
