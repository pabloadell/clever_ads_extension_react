# Keyword Planner Chrome Extension

## Bump version

To update the version of the extension use the command =yarn version=
https://classic.yarnpkg.com/en/docs/cli/version/#toc-commands

There is a script that will automatically update the manifest.json located in the public folder

## Release a version

=yarn release= will generate a .zip needed to upload to the chrome store
