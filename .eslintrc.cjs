// module.exports = {
//   "env": {
//     "node": true,
//     "es6": true,
//     "browser": true
//   },
//   "extends": ["plugin:react/recommended", "eslint:recommended", "airbnb", "react-app"],
//   "plugins": ["react"],
//   "parser": "babel-eslint",
//   "parserOptions": {
//     "ecmaVersion": 2018,
//     "ecmaFeatures": {
//       "jsx": true,
//       "js": true,
//       "modules": true
//     },
//     "sourceType": "module"
//   },
//   "settings": {
//     "react": {
//       "version": "17.0.1"
//     },
//     "import/resolver": {
//       "node": {
//         "paths": ["src"],
//       }
//     }
//   },
//   "rules": {
//     "react/jsx-fragments": 0,
//     "no-param-reassign": 1,
//     "react/no-multi-comp": 0,
//     "comma-dangle": 1,
//     "no-restricted-globals": 1,
//     "no-useless-escape": 0,
//     "camelcase": 1,
//     "operator-linebreak": 0,
//     "jsx-no-bind": 0,
//     "react/sort-comp": 0,
//     "import/prefer-default-export": 0,
//     "import/named": 0,
//     "react/jsx-one-expression-per-line": 0,
//     "react/prefer-stateless-function": [1, { "ignorePureComponents": true }],
//     "import/no-extraneous-dependencies": ["error", { "devDependencies": true }],
//     "no-plusplus": 0,
//     "no-else-return": 0,
//     "react/destructuring-assignment": [1, "always", { "ignoreClassFields": true }],
//     "max-len": 0,
//     "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
//     "react/prop-types": [0],
//     "react/jsx-uses-vars": [2],
//     "no-extra-boolean-cast": 0,
//     "object-curly-newline": ["error", { "multiline": true, "minProperties": 7 }],
//     "arrow-parens": [2, "as-needed"],
//     "no-console": [
//       "error",
//       {
//         "allow": ["log", "error"]
//       }
//     ],
//     "indent": [
//       "error",
//       2,
//       { "SwitchCase": 1 }
//     ],
//     "linebreak-style": [
//       "error",
//       "unix"
//     ],
//     "quotes": [
//       "error",
//       "double"
//     ],
//     "semi": [
//       "error",
//       "always"
//     ]
//   }
// };

const RULE = {
  OFF: "off",
  WARN: "warn",
  ERROR: "error",
};

module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
    "jest/globals": true,
  },
  extends: [
    "plugin:react/recommended",
    "airbnb",
    "plugin:jest/recommended",
    "plugin:react-hooks/recommended",
    "plugin:testing-library/react",
    "prettier",
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 11,
    sourceType: "module",
  },
  settings: {
    "import/resolver": {
      node: {
        paths: ["src"],
        moduleDirectory: ["node_modules", "src/"],
      },
    },
    jest: { version: 26 },
  },
  plugins: ["react", "jest", "react-hooks", "testing-library", "import"],
  rules: {
    "no-param-reassign": RULE.OFF,
    "react/destructuring-assignment": RULE.WARN,
    "react/no-unused-prop-types": RULE.WARN,
    "react/no-array-index-key": RULE.OFF,
    "no-unused-vars": [RULE.WARN, { argsIgnorePattern: "^_" }],
    "react/require-default-props": RULE.OFF,
    "react/jsx-props-no-spreading": RULE.OFF,
    "object-curly-newline": RULE.OFF,
    "react/jsx-filename-extension": [
      RULE.ERROR,
      { extensions: [".js", ".jsx"] },
    ],
    "react/prop-types": RULE.OFF,
    "arrow-parens": [RULE.ERROR, "as-needed"],
    "jsx-a11y/anchor-is-valid": [
      RULE.WARN,
      {
        components: ["Link"],
        aspects: ["invalidHref"],
      },
    ],
    "max-len": ["error", { code: 130 }],
    "react/jsx-one-expression-per-line": [RULE.OFF],
    "import/no-extraneous-dependencies": [RULE.OFF],
  },
};
