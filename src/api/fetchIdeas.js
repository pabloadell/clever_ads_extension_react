import { fetchJson } from "./utils";

// eslint-disable-next-line
export const getKeywords = url => fetchJson("fetch_ideas", { url }, false);
