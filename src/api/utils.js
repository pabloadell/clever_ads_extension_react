/* eslint-disable no-empty */
/* eslint-disable no-undef */

import axios from "axios";
import reduce from "lodash-es/reduce";

import { resolveCamelCaseObject, resolveSnakeCaseObject } from "./helpers";
import { API_URL } from "../utils/environment";

function getBaseHeaders() {
  return { "Content-Type": "application/json" };
}

function buildQueryParams(query) {
  return reduce(
    Object.keys(query),
    (buildQuery, key) => {
      if (query[key] === undefined || query[key] === null) return buildQuery;

      let res = buildQuery;

      if (res.length > 0) res += "&";

      return `${res}${key}=${query[key]}`;
    },
    ""
  );
}

function buildUrl(url, baseUrl) {
  return `${baseUrl}${url}`;
}

/* eslint-disable no-console, no-unused-vars */
function log(response) {
  let logger;

  if (console && console.log) {
    logger = console.log;
  } else {
    logger = string => {};
  }

  try {
    if (Rollbar !== undefined && Rollbar !== null) {
      logger = Rollbar.error;
    }

    logger(`Error in request ${response.url} with status ${response.status}`);
  } catch (e) {}
}
/* eslint-enable no-console, no-unused-vars */

function parseResponse(response) {
  try {
    if (response.status === 401 || response.status === 403) {
      log(response);

      return { error: "Unauthorized", status: 401 };
    }
    if (response.status !== 200) {
      log(response);

      return { error: "Error in response", status: 500 };
    }
    return response.data;
  } catch (e) {
    log(response);

    return { error: "Error in response", status: 500 };
  }
}

const get = async (url, query, baseUrl) => {
  const queryParams = buildQueryParams(query);
  let requestUrl = buildUrl(url, baseUrl);

  if (queryParams.length > 0) requestUrl += `?${queryParams}`;

  const response = await axios.get(requestUrl, {
    headers: { ...getBaseHeaders() },
  });

  return parseResponse(response);
};

const post = async (url, data, baseUrl) => {
  const response = await axios({
    url: buildUrl(url, baseUrl),
    method: "post",
    headers: { ...getBaseHeaders() },
    data: resolveSnakeCaseObject(data),
  });

  return parseResponse(response);
};

const requestBuilder =
  request =>
  async (url, data = {}, parseToCamel = true, baseUrl = API_URL) => {
    try {
      const response = await request(url, data, baseUrl);

      if (parseToCamel) return resolveCamelCaseObject(response);
      return response;
    } catch (e) {
      log({ url, status: -1 });

      return { err: `Internal Error: ${e.toString()}` };
    }
  };

export const fetchJson = requestBuilder(get);
export const postJson = requestBuilder(post);
