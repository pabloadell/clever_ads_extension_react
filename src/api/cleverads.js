import { postJson, fetchJson } from "./utils";
import { API_CLEVERADS } from "../utils/environment";

export const getKeywordsFileBySelected = (type, keywords, headers) =>
  postJson(
    "/keywords/download",
    { type, keywords, headers },
    false,
    API_CLEVERADS
  );

export const getKeywordsIdSearch = url =>
  fetchJson(
    "keywords_stream",
    { keywords: url, location: null, language: null },
    false,
    API_CLEVERADS
  );

export const canGenerateKeywords = url =>
  fetchJson("utils/can_generate_keywords", { uri: url }, false, API_CLEVERADS);
