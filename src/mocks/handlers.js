import { rest } from "msw";
import { API_URL, API_CLEVERADS } from "../utils/environment";

import fetchIdeasMock from "./fetch_ideas_response.json";

// eslint-disable-next-line
export const handlers = [
  rest.post(`${API_CLEVERADS}keywords/download`, (req, res, ctx) =>
    res(ctx.body(""))
  ),

  rest.get(`${API_CLEVERADS}keywords_stream`, (req, res, ctx) =>
    res(ctx.json({ job_id: "123456" }))
  ),

  rest.get(`${API_CLEVERADS}utils/can_generate_keywords`, (req, res, ctx) =>
    res(ctx.delay(300), ctx.json({ can_generate_keywords: true }))
  ),

  rest.get(`${API_URL}fetch_ideas`, (req, res, ctx) =>
    res(ctx.delay(600), ctx.status(200), ctx.json(fetchIdeasMock))
  ),
];
