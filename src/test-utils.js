import React from "react";
import { render } from "@testing-library/react";

import ThemeProvider from "clever-react/ui/Provider";
import { getCurrentConfig } from "clever-react/ui/Provider/themeConfig";

const AllTheProviders = ({ children }) => (
  <ThemeProvider theme={getCurrentConfig}>{children}</ThemeProvider>
);

const customRender = (ui, options) =>
  render(ui, { wrapper: AllTheProviders, ...options });

// re-export everything
export * from "@testing-library/react";

// override render method
export { customRender as render };
