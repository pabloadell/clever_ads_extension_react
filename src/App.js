import React from "react";

import ThemeProvider from "clever-react/ui/Provider";

import "./App.css";

import { getCurrentConfig } from "clever-react/ui/Provider/themeConfig";
import MainPage from "./components/MainPage";
import { AppProvider } from "./utils/context";

if (process.env.NODE_ENV === "development") {
  const { worker } = require("./mocks/browser"); // eslint-disable-line
  worker.start();
}

const App = () => (
  <ThemeProvider theme={getCurrentConfig}>
    <AppProvider>
      <div className="App background">
        <MainPage />
      </div>
    </AppProvider>
  </ThemeProvider>
);

export default App;
