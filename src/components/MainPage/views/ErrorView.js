import React from "react";

import Typography from "clever-react/ui/Typography";
import Grid from "clever-react/ui/Grid";
import Image from "clever-react/ui/Image";
import makeStyles from "clever-react/ui/styles/makeStyles";
import Link from "clever-react/ui/Link";

import warningSvg from "../../../images/warning.svg";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  scanpage: { ...spacing["mt-6"] },
  loadingview: { ...spacing["my-7"] },
  loadingtext: { ...spacing["mt-4"] },
  footer: {
    ...spacing["mt-6"],
    ...spacing["mb-5"],
    "& *": { display: "inline" },
  },
  text: { display: "inline" },
}));

const ErrorView = () => {
  const classes = getClasses();

  return (
    <Grid container justify="center">
      <Grid item xs={12} className={classes.loadingview}>
        <Image src={warningSvg} alt="" size={225} />
        <Typography
          className={classes.loadingtext}
          variant="f2-16"
          shade={600}
        >
          Unable to analyze domain. Please try another one.
        </Typography>
      </Grid>

      <div className={classes.footer}>
        <Typography className={classes.text} variant="f2-14" shade={700}>
          Find out more at{" "}
        </Typography>
        <Link
          href="https://cleverads.com/keyword-planner/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <Typography
            className={classes.link}
            color="primary"
            variant="f2-14"
            weight="bold"
          >
            Clever Ads
          </Typography>
        </Link>
      </div>
    </Grid>
  );
};

export default ErrorView;
