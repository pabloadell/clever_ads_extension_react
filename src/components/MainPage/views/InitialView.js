import React from "react";

import Typography from "clever-react/ui/Typography";
import Grid from "clever-react/ui/Grid";
import Image from "clever-react/ui/Image";
import makeStyles from "clever-react/ui/styles/makeStyles";

import woman from "../../../images/illustration-woman-with-treasure.svg";
import loadingGif from "../../../images/ellipsis.gif";

import ScanButton from "../../ScanButton";
import LinkArrow from "../components/LinkArrow";

import { useKWPState } from "../../../utils/context";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  scanpage: { ...spacing["mt-6"] },
  loadingview: { ...spacing["my-7"] },
  loadingtext: { ...spacing["mt-4"] },
  footer: { ...spacing["mt-6"], ...spacing["mb-5"] },
  text: { display: "inline" },
}));

const InitialView = ({ loadKeywords }) => {
  const classes = getClasses();
  const { isLoading, keywords } = useKWPState();

  return (
    <Grid container justify="center">
      {!!isLoading && (
        <Grid item xs={12} className={classes.loadingview}>
          <Image src={loadingGif} alt="loading keywords" size={225} />
          <Typography className={classes.loadingtext} variant="f2-18">
            Preparing your keywords...
          </Typography>
        </Grid>
      )}

      {keywords.length === 0 && !isLoading && (
        <Grid item xs={12} className={classes.scanpage}>
          <Typography
            variant="f1-18"
            weight="bold"
            align="center"
            shade={500}
          >
            Let&apos;s find your keywords
          </Typography>

          <Image src={woman} alt="woman with a treasure" size={250} />

          <ScanButton onClick={loadKeywords} />
        </Grid>
      )}

      <div className={classes.footer}>
        <Typography className={classes.text} variant="f2-14" shade={700}>
          Discover more about{" "}
        </Typography>
        <LinkArrow
          href="https://cleverads.com/keyword-planner/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <Typography
            className={classes.link}
            color="primary"
            variant="f2-14"
            weight="bold"
          >
            Clever Ads Keyword Planner
          </Typography>
        </LinkArrow>
      </div>
    </Grid>
  );
};

export default InitialView;
