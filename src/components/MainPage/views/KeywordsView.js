/* global chrome */
import React from "react";

import Typography from "clever-react/ui/Typography";
import Grid from "clever-react/ui/Grid";
import makeStyles from "clever-react/ui/styles/makeStyles";
import Image from "clever-react/ui/Image";

import { KWPCleverAdsURL } from "../../../utils/environment";
import DownloadButton from "../components/DownloadButton";
import DataTable from "../../DataTable";
import LinkArrow from "../components/LinkArrow";

import ModalAuthInfo from "../components/ModalAuthInfo";

import loadingGif from "../../../images/ellipsis.gif";

import { useKWPState } from "../../../utils/context";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  doneHeading: { ...spacing["my-5"] },
  description: { ...spacing["mb-5"] },
  downloadButtons: { ...spacing["mt-4"] },
  datatable: { ...spacing["mb-6"], ...spacing["mx-2"] },
  loadingtext: { fontSize: "10pt" },
  numberKeywordsClass: { fontWeight: "bold" },
  footer: { ...spacing["mt-6"], ...spacing["mb-5"] },
  textInline: { display: "inline" },
}));

const openNewTab = url => {
  if (process.env.NODE_EV === "production") {
    chrome.tabs.create({ url });
  } else {
    window.open(url);
  }
};

const KeywordsView = () => {
  const classes = getClasses();
  const [openModal, setOpenModal] = React.useState(false);
  const [modalUrl, setModalUrl] = React.useState(KWPCleverAdsURL);

  const KWPState = useKWPState();
  const numberKeywords = KWPState.keywords.length;
  const { isLoading, keywords } = KWPState;

  const handleDownload = () => {
    KWPState.downloadFn();
  };

  const handleManageKeywords = ev => {
    ev.preventDefault();

    setModalUrl(`${KWPCleverAdsURL}?id=${KWPState.keywordsSearchId}`);
    setOpenModal(true);
  };

  const handleGoToKWP = ev => {
    ev.preventDefault();

    setModalUrl(KWPCleverAdsURL);
    setOpenModal(true);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const handleContinueModal = () => {
    openNewTab(modalUrl);
  };

  return (
    <Grid container justify="center">
      <ModalAuthInfo
        open={openModal}
        onClose={handleCloseModal}
        onAccept={handleContinueModal}
      />

      <Grid item xs={10} className={classes.doneHeading}>
        <Typography variant="h1" weight="bold" shade={900}>
          Done!
        </Typography>
      </Grid>

      <Grid item xs={10} className={classes.description}>
        <Typography className={classes.textInline} variant="f2-16" shade={700}>
          Clever Ads found{" "}
          <span className={classes.numberKeywordsClass}>{numberKeywords} </span>{" "}
          unique keyword suggestions on this site.{" "}
        </Typography>

        <LinkArrow onClick={handleManageKeywords}>
          <Typography color="primary" variant="f2-16" weight="bold">
            Login to Clever Ads to manage them
          </Typography>
        </LinkArrow>
      </Grid>

      {!!isLoading && (
        <Grid item xs={12} className={classes.loadingview}>
          <Image src={loadingGif} alt="loading keywords" size={125} />
          <Typography className={classes.loadingtext} variant="body1">
            Organizing your keywords...
          </Typography>
        </Grid>
      )}

      {!isLoading && (
        <Grid item xs={12} className={classes.datatable}>
          <DataTable keywords={keywords} className={classes.tableAppeareance} />
        </Grid>
      )}

      {!!keywords && !isLoading && (
        <Grid xs={12} item className={classes.downloadButtons}>
          <DownloadButton
            onClick={handleDownload}
            isLoading={KWPState.downloadLoading}
          />
        </Grid>
      )}

      <Grid item xs={10} className={classes.footer}>
        <Typography className={classes.textInline} variant="f2-14" shade={500}>
          Manage, filter, and group suggested keywords in lists and get them
          uploaded to your Google Ads account directly from Clever Ads.{" "}
        </Typography>
        <LinkArrow onClick={handleGoToKWP}>
          <Typography color="primary" variant="f2-14" weight="bold">
            Login to Clever Ads Keyword Planner
          </Typography>
        </LinkArrow>
      </Grid>
    </Grid>
  );
};

export default KeywordsView;
