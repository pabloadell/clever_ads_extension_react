/* global chrome */
import React, { useState } from "react";

import Grid from "clever-react/ui/Grid";

import Header from "./components/Header";

import KeywordsView from "./views/KeywordsView";
import InitialView from "./views/InitialView";
import ErrorView from "./views/ErrorView";

import { useKWPState } from "../../utils/context";

const MainPageHeader = ({ children }) => (
  <Grid container justify="center">
    <Grid item xs={12}>
      <Header />
    </Grid>

    {children}
  </Grid>
);

const MainPage = () => {
  const { getKeywordsFn, isLoading, keywords, isRejected } = useKWPState();

  const [urlPage, setUrlPage] = useState("");

  const loadKeywords = async () => {
    const queryInfo = { active: true, currentWindow: true };

    if (process.env.NODE_ENV === "production") {
      try {
        chrome.tabs.query(queryInfo, async tabs => {
          const { url } = tabs[0];

          getKeywordsFn(url);

          setUrlPage(url);
        });
      } catch (e) {
        setUrlPage("");
      }
    } else {
      const url = "https://cleverads.com";
      setUrlPage(url);
      getKeywordsFn(url);
    }
  };

  if (isRejected) {
    return (
      <MainPageHeader>
        <ErrorView />
      </MainPageHeader>
    );
  }

  return (
    <Grid container justify="center">
      <Grid item xs={12}>
        <Header />
      </Grid>

      {keywords.length > 0 && !isLoading ? (
        <KeywordsView urlPage={urlPage} />
      ) : (
        <InitialView loadKeywords={loadKeywords} />
      )}
    </Grid>
  );
};

export default MainPage;
