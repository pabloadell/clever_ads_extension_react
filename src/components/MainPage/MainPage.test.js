import React from "react";
import "@testing-library/jest-dom";

import { render, screen, waitFor } from "test-utils";
import userEvent from "@testing-library/user-event";
import { server, rest } from "mocks/server";
import { API_CLEVERADS } from "utils/environment";

import { AppProvider } from "utils/context";
import MainPage from "./index";

describe("<MainPage />", () => {
  it("should render correctly", () => {
    render(
      <AppProvider>
        <MainPage />
      </AppProvider>
    );

    expect(screen.getByText(/Scan this page/i)).toBeInTheDocument();
  });

  it("search keywords correctly", async () => {
    render(
      <AppProvider>
        <MainPage />
      </AppProvider>
    );

    const scanButton = screen.getByText(/Scan this page/i);

    userEvent.click(scanButton);

    expect(screen.getByText(/Preparing your keywords/i)).toBeInTheDocument();

    await waitFor(() =>
      expect(screen.getByText(/Clever Ads found/i)).toBeInTheDocument()
    );
  });

  it("shows error when the url is not capable of generating keywords", async () => {
    server.use(
      rest.get(`${API_CLEVERADS}utils/can_generate_keywords`, (req, res, ctx) =>
        res(ctx.delay(500), ctx.json({ can_generate_keywords: false }))
      )
    );
    render(
      <AppProvider>
        <MainPage />
      </AppProvider>
    );

    const scanButton = screen.getByText(/Scan this page/i);

    userEvent.click(scanButton);

    expect(screen.getByText(/Preparing your keywords/i)).toBeInTheDocument();

    await waitFor(() =>
      expect(
        screen.getByText(/Unable to analyze domain. Please try another one./i)
      ).toBeInTheDocument()
    );
  });

  it("shows error when the petition to check URL fails", async () => {
    server.use(
      rest.get(`${API_CLEVERADS}utils/can_generate_keywords`, (req, res, ctx) =>
        res(ctx.delay(500), ctx.status(404))
      )
    );
    render(
      <AppProvider>
        <MainPage />
      </AppProvider>
    );

    const scanButton = screen.getByText(/Scan this page/i);

    userEvent.click(scanButton);

    expect(screen.getByText(/Preparing your keywords/i)).toBeInTheDocument();

    await waitFor(() =>
      expect(
        screen.getByText(/Unable to analyze domain. Please try another one/i)
      ).toBeInTheDocument()
    );
  });
});
