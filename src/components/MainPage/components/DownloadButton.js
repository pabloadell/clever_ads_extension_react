import React from "react";
import Loading from "clever-react/ui/Loading";
import Button from "clever-react/ui/Button";
import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  root: { ...spacing["mt-3"], ...spacing["mx-auto"], maxWidth: "250px" },
}));

const DownloadButton = ({ onClick, disabled, isLoading }) => {
  const classes = getClasses();
  return (
    <Button
      variant="primary"
      className={classes.root}
      onClick={onClick}
      disabled={disabled || isLoading}
    >
      Download All
      {isLoading && <Loading size={20} />}
    </Button>
  );
};

export default DownloadButton;
