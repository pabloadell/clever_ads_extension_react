import React, { Fragment } from "react";

import Button from "clever-react/ui/Button";
import Dialog from "clever-react/ui/Dialog";
import Link from "clever-react/ui/Link";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { spacing }, breakpoints }) => ({
  overflowDialog: { overflowWrap: "break-word" },
  warningImg: {
    ...spacing["mt-4"],
    textAlign: "center",
    [breakpoints.down("sm")]: { display: "none" },
  },
  linkCancel: { ...spacing["my-4"] },
}));

const ModalAuthInfo = props => {
  const classes = getClasses();
  const { open, onClose, onAccept } = props;

  return (
    <Dialog
      className={classes.overflowDialog}
      open={open}
      onClose={onClose}
      maxWidth="md"
      title="First things first: we've got your back"
      description={
        // eslint-disable-next-line
        "We'll ask you to sign into your Google Ads account to create your keywords list, this way you'll always be ble to come back and manage your keyword research lists.\n\nWe take data privacy seriously and only collect what we need to provide you with the best experience possible."
      }
      image="https://res.cloudinary.com/cleverppc/image/upload/v1603454811/CleverAds/illustration-padlock.svg"
      buttons={
        <>
          <Button variant="primary" onClick={onAccept}>
            Continue
          </Button>
          <Link className={classes.linkCancel} onClick={onClose}>
            Cancel
          </Link>
        </>
      }
    />
  );
};

export default React.memo(ModalAuthInfo);
