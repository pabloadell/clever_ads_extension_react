import React from "react";

import Grid from "clever-react/ui/Grid";
import Typography from "clever-react/ui/Typography";
import Image from "clever-react/ui/Image";

import makeStyles from "clever-react/ui/styles/makeStyles";

import logo from "../../../images/logo-cleverads.svg";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  root: { backgroundColor: "white", ...spacing["p-3"] },
  cleverheader: { ...spacing["mr-3"] },
  logo: { ...spacing["ml-5"], ...spacing["mr-4"] },
  headerImage: { maxHeight: "40px" },
  headerText: { fontSize: "15pt !important", ...spacing["pt-1"] },
}));

const Header = () => {
  const classes = getClasses();

  return (
    <Grid container className={classes.root} alignItems="center">
      <Grid item className={classes.logo}>
        <Image src={logo} className={classes.headerImage} center={false} />
      </Grid>
      <Grid item>
        <Typography variant="f2-20" className={classes.headerText}>
          Keyword Planner
        </Typography>
      </Grid>
    </Grid>
  );
};

export default Header;
