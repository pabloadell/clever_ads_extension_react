import React from "react";

import makeStyles from "clever-react/ui/styles/makeStyles";
import ArrowRight from "clever-react/ui/Icon/ArrowRight";

const getClasses = makeStyles(({ cleverUI: { spacing, colors } }) => ({
  link: {
    display: "inline-flex",
    alignItems: "center",
    textDecoration: "none",
    color: colors.primary.main,
    "&:hover": { textDecoration: "underline" },
  },
  arrow: { ...spacing["ml-1"] },
}));

const defaultOnClick = () => {};

const LinkArrow = ({
  href = "",
  target = "",
  rel = "",
  onClick = defaultOnClick,
  children,
}) => {
  const classes = getClasses();

  return (
    <a
      className={classes.link}
      href={href}
      target={target}
      rel={rel}
      onClick={onClick}
    >
      {children}
      <ArrowRight className={classes.arrow} size={12} color="primary" />
    </a>
  );
};

export default LinkArrow;
