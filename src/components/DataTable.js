import React from "react";

import DataTableExt from "clever-react/ui/DataTable";

const tableHeaders = [
  {
    Header: "Keyword",
    accessor: "keyword",
    fixedWidth: "55%",
  },
  {
    Header: "Search Volume",
    accessor: "searchVolume",
    align: "right",
    fixedWidth: "15%",
    sortDescFirst: true,
  },
  {
    Header: "CPC",
    accessor: "avgCpc",
    align: "right",
    fixedWidth: "30%",
  },
];

const defaultSorting = [
  {
    id: "searchVolume",
    desc: true,
  },
];

const DataTable = ({ keywords }) => (
  <DataTableExt
    data={keywords}
    headers={tableHeaders}
    fullWidth
    defaultSorting={defaultSorting}
    disableSortRemove
    rowHover={false}
  />
);

export default DataTable;
