import React from "react";

import Button from "clever-react/ui/Button";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  root: { ...spacing["pt-3"] },
  scanButton: { width: "245px", ...spacing["px-5"] },
  fontWeight: "bold",
}));

const ScanButton = ({ onClick }) => {
  const classes = getClasses();

  return (
    <div className={classes.root} id="checkPageContainer">
      <Button
        id="checkPage"
        onClick={onClick}
        align="center"
        className={classes.scanButton}
      >
        Scan this page
      </Button>
    </div>
  );
};

export default ScanButton;
