import { MAX_ROWS } from "./environment";

export const HEADERS = ["Keyword", "Monthly Search Volume", "Avg. CPC"];

const createRow = keywordData => ({
  keyword: keywordData.keyword,
  searchVolume: Number(keywordData.search_volume),
  avgCpc: `$${(
    parseFloat(keywordData.avg_cpc.micro_amount, 2) / 1000000
  ).toFixed(2)}`,
});

const validKeywordData = keywordData =>
  keywordData !== null && keywordData.avg_cpc != null;

export const generateCSVData = keywords => {
  const headersStr = `${HEADERS.join(", ")}\r\n`;

  const dataCsv = keywords.reduce((previous, keywordObject) => {
    const keywordRowString = `${Object.values(keywordObject).join(", ")}\r\n`;

    return previous + keywordRowString;
  }, headersStr);

  return dataCsv;
};

export const parseKeywords = keywords => {
  const { length } = keywords;
  const maxRows = length > MAX_ROWS ? MAX_ROWS : length;
  const data = [];

  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < maxRows; i++) {
    if (validKeywordData(keywords[i][0])) {
      const row = createRow(keywords[i][0]);
      data.push(row);
    }
  }
  return data;
};

export const downloadZIPBase64 = (zipBase64, filename) => {
  const tempLink = document.createElement("a");

  tempLink.href = `data:application/zip;base64,${zipBase64}`;
  tempLink.download = `${filename}.zip`;
  tempLink.click();

  tempLink.remove();
};

export const generateFileName = url =>
  `keywords_cleverads_${url
    .split("/")[2]
    .split(".")[0]
    .replace(/[^-A-Za-z0-9+&@#/%?=~_|!:,.;()]/g, "")}`;
