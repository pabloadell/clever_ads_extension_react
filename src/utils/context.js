import React from "react";
import produce from "immer";

import { getKeywords } from "../api/fetchIdeas";
import {
  getKeywordsFileBySelected,
  getKeywordsIdSearch,
  canGenerateKeywords,
} from "../api/cleverads";

import {
  parseKeywords,
  downloadZIPBase64,
  HEADERS,
  generateFileName,
} from "./utils";

const FETCH_IDEAS_INIT = "FETCH_IDEAS_INIT";
const FETCH_IDEAS_FINISH = "FETCH_IDEAS_FINISH";
const FETCH_IDEAS_ERROR = "FETCH_IDEAS_ERROR";
const DOWNLOAD_ZIP_INIT = "DOWNLOAD_PDF_INIT";
const DOWNLOAD_ZIP_FINISH = "DOWNLOAD_PDF_FINISH";

export const status = Object.freeze({
  IDLE: "idle",
  PENDING: "pending",
  RESOLVED: "resolved",
  REJECTED: "rejected",
});

const launchAction = (dispatch, action, state) => {
  dispatch({ type: action, payload: state });
};

export const getKeywordsAction = url => async dispatch => {
  launchAction(dispatch, FETCH_IDEAS_INIT, { url });

  const urlCheck = await canGenerateKeywords(url);

  if (!urlCheck.can_generate_keywords) {
    launchAction(dispatch, FETCH_IDEAS_ERROR, {});
    return;
  }

  const responsePromise = getKeywords(url);
  const keywordsSearchIdPromise = getKeywordsIdSearch(url);

  const [response, keywordsSearchId] = await Promise.all([
    responsePromise,
    keywordsSearchIdPromise,
  ]);

  const keywordsParsed = parseKeywords(response.data);

  launchAction(dispatch, FETCH_IDEAS_FINISH, {
    keywords: keywordsParsed,
    keywordsSearchId: keywordsSearchId?.job_id,
  });
};

export const doDownloadSelectedInZip = (
  keywords,
  filename
) => async dispatch => {
  launchAction(dispatch, DOWNLOAD_ZIP_INIT, {});

  // Parse the array of object to an array of arrays needed for the download api
  const keywordsParsedApi = keywords.map(keyword => Object.values(keyword));

  const dataRaw = await getKeywordsFileBySelected(
    "zip",
    keywordsParsedApi,
    HEADERS
  );

  downloadZIPBase64(dataRaw, filename);

  launchAction(dispatch, DOWNLOAD_ZIP_FINISH, {});
};

const defaultState = {
  status: status.IDLE,
  keywords: [],
  url: "",
  downloadStatus: status.IDLE,
  keywordsSearchId: "",
};

/* eslint no-param-reassign: "error" */
export const keywordPlannerReducer = (state, action) =>
  produce(state || defaultState, possibleState => {
    switch (action.type) {
      case FETCH_IDEAS_INIT: {
        possibleState.status = status.PENDING;
        possibleState.url = action.payload.url;
        break;
      }
      case FETCH_IDEAS_FINISH: {
        possibleState.status = status.RESOLVED;
        possibleState.keywords = action.payload.keywords;
        possibleState.keywordsSearchId = action.payload.keywordsSearchId ?? "";
        break;
      }
      case FETCH_IDEAS_ERROR:
        possibleState.status = status.REJECTED;
        break;
      case DOWNLOAD_ZIP_INIT: {
        possibleState.downloadStatus = status.PENDING;
        break;
      }
      case DOWNLOAD_ZIP_FINISH: {
        possibleState.downloadStatus = status.RESOLVED;
        break;
      }
      default: {
        // Do nothing
      }
    }
  });

const AppStateContext = React.createContext();
const AppDispatchContext = React.createContext();

const AppProvider = props => {
  const { children } = props;
  const [state, dispatch] = React.useReducer(
    keywordPlannerReducer,
    defaultState
  );

  return (
    <AppStateContext.Provider value={state}>
      <AppDispatchContext.Provider value={dispatch}>
        {children}
      </AppDispatchContext.Provider>
    </AppStateContext.Provider>
  );
};

function useAppState() {
  const context = React.useContext(AppStateContext);
  if (context === undefined) {
    throw new Error("useAppContext must be within AppProvider");
  }

  return context;
}

function useDispatch() {
  const context = React.useContext(AppDispatchContext);
  if (context === undefined) {
    throw new Error("useAppDispatch must be within AppProvider");
  }

  return context;
}

function useKWPState() {
  const state = useAppState();
  const dispatch = useDispatch();

  const getKeywordsFromUrl = React.useCallback(
    url => {
      getKeywordsAction(url)(dispatch);
    },
    [dispatch]
  );

  const downloadKeywords = React.useCallback(() => {
    const filename = generateFileName(state.url) ?? "keywords_cleverads";

    doDownloadSelectedInZip(state.keywords, filename)(dispatch);
  }, [dispatch, state.keywords, state.url]);

  return {
    keywords: state.keywords,
    keywordsSearchId: state.keywordsSearchId,
    isLoading: state.status === status.PENDING,
    downloadLoading: state.downloadStatus === status.PENDING,
    isRejected: state.status === status.REJECTED,
    getKeywordsFn: getKeywordsFromUrl,
    downloadFn: downloadKeywords,
  };
}

export { AppProvider, useKWPState };
