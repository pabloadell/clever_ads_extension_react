export const API_URL = "https://keyword-ideas.cleverecommerce.com/api/v1/";
export const API_CLEVERADS = "https://cleverads.com/api/v1/";
export const MAX_ROWS = "350";
export const KWPCleverAdsURL = "https://cleverads.com/app/keyword_planner";
